﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestExecutionOrder : MonoBehaviour
{

    public int NumeroDeTest = 0;

    private void Awake()
    {
        Debug.Log("Awake" + NumeroDeTest);
    }

    private void Update()
    {
        Debug.LogWarning("OnEnable" + NumeroDeTest);
    }

    private void LateUpdate()
    {
        Debug.LogError("Start" + NumeroDeTest);
    }

}
